import os
import json
import requests
from requests.auth import HTTPBasicAuth
from flask import Flask, make_response, request, Blueprint
import app

jitbit_api = Blueprint('jitbit_api', __name__, url_prefix='/jitbit')

helpdesk_url = os.getenv("HELPDESK_URL")

# @jitbit_api.route('/getUnclosedTickets/<username>', methods=['GET'])
def get_unclosed_tickets(username):
    url = helpdesk_url + '/Helpdesk/api/Tickets'
    parameters = {'mode': 'Unclosed', 'count': '250'}
    response = requests.get(url, params=parameters,
    auth=HTTPBasicAuth(username, '')).json()
    return response

# @jitbit_api.route('/getAllTickets/<username>', methods=['GET'])
def get_all_tickets(username):
    url = helpdesk_url + '/Helpdesk/api/Tickets'
    parameters = {'mode': 'all', 'count': '500'}
    response = requests.get(url, params=parameters,
    auth=HTTPBasicAuth(username, '')).json()
    return response

# @jitbit_api.route('/getTicket/<ticket_number>/<username>', methods=['GET'])
def get_ticket(username, ticket_number):
    url = helpdesk_url + '/Helpdesk/api/ticket'
    parameters = {'id': ticket_number}
    response = requests.get(url, params=parameters,
    auth=HTTPBasicAuth(username, '')).json()
    return response

# @jitbit_api.route('/getTicketComments/<ticket_number>/<username>/', methods=['GET'])
def get_ticket_comments(username, ticket_number):
    url = helpdesk_url + '/Helpdesk/api/comments'
    parameters = {'id': ticket_number}
    response = requests.get(url,
    params=parameters,
    auth=HTTPBasicAuth(username, '')).json()
    return response

# @jitbit_api.route('/getTechs/<username>/', methods=['GET'])
def get_techs(username):
    url = helpdesk_url + '/helpdesk/api/users'
    # parameters = {'count': 50, 'listMode': 'techs'}
    parameters = {}
    response = requests.get(url, params=parameters,
    auth=HTTPBasicAuth(username, '')).json()
    return response
