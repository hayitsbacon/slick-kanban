import os
from flask import Flask, render_template
from slackclient import SlackClient
from flask_sqlalchemy import SQLAlchemy

flask = Flask(__name__)
flask.config.from_object('config')

db = SQLAlchemy(flask)

@flask.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@flask.route('/')
def index():
   return "Put /NetID at the end of the URL"

from app.mod_jitbit.api import jitbit_api 
from app.mod_kanban.board import kanban

flask.register_blueprint(jitbit_api)
flask.register_blueprint(kanban)

db.create_all()
