import os
import requests
import json
import datetime
import re
from requests.auth import HTTPBasicAuth
from flask import Flask, make_response, request, Blueprint, render_template
import app
from app.mod_jitbit.api import *

kanban = Blueprint('kanban_api', __name__)

@kanban.route('/<username>')
def ticket_board(username):
    unclosed_tickets = get_unclosed_tickets(username)
    all_tickets = get_all_tickets(username)
    tech_tickets = []
    queue_tickets = []
    working_tickets = []
    closed_tickets = []

    for list in unclosed_tickets:
        if (list['Technician'] is not None): 
            ticket_last_updated = get_ticket_last_updated(list)
            updated_today = (datetime.datetime.now() -
            datetime.datetime.strptime(ticket_last_updated, "%Y-%m-%dT%H:%M:%S") <
            datetime.timedelta(days=2))
            if (username in list['Technician']):
                if (updated_today):
                    working_tickets.append(list)
                if (not updated_today):
                    queue_tickets.append(list)
                tech_tickets.append(list)

    for list in all_tickets:
        if ('Resolved' in list['Status']):
            if (list['Technician'] is not None):
                today = datetime.datetime.now()
                monday = today - datetime.timedelta(days=today.weekday())
                last_updated = datetime.datetime.strptime(get_ticket_last_updated(list), "%Y-%m-%dT%H:%M:%S")
                if (monday < last_updated):
                    if (username in list['Technician']):
                        closed_tickets.append(list)

    return render_template('index.html',
    tech_tickets=tech_tickets,
    queue_tickets=queue_tickets,
    working_tickets=working_tickets,
    closed_tickets=closed_tickets,
    helpdesk_url=helpdesk_url)

def get_ticket_last_updated(ticket):
    last_updated = ticket['LastUpdated']
    response = datetime.datetime.fromtimestamp(int(re.sub('\D', '', last_updated)[:10])).isoformat()
    return response 
